#  # Génerateur des tags depuis plugin/paquet.xml

## Introduction

> Le script analyse un depot git et identife les différents tags à créer depuis les numéros de versions saisies dans pluggn et paquet.xml

Pour chaque commit le script identifie le numéro de version présent dans plugin.xml puis dans paquet.xml. Si paquet.xml est présent il sera prioritaire.

Il fonctionne pour n'importe quel plugin versioné avec git

## Usage

```
git clone https://git.spip.net/spip-contrib-outils/creer_tags.git
cd creer_tags
./set_tags_from_paquet.sh ../chemin/plugin/spip
# Pour pousser les tags disponibles
git push --tags
```

## Exemple

```
git clone https://git.spip.net/spip-contrib-extensions/saisies.git saisies
bash creer_tags/set_tags_from_paquet.sh saisies
../saisies
v1.42.5 at e95ee4e7735201d9a474b176f1b65590cffeab36
v2.0.0 at 120ab905a072ed6f97a6ad084ef0e894f2f4f5aa
v2.0.1 at 60e2f48fa03c0ffaca4acdd36adc9882286c7fb5
v2.0.2 at c442efc1139bb13a383f9a1021f70052d1548a62
v2.0.3 at d3a01f5b43c955aab4b13ff797f868693b4e95aa
v2.0.4 at 1d9112e4db15a7b81a31bf7e5af7c5f6701bcada
v2.0.5 at 2c0309110aeed62c0a2ba4f72034b2847c264a76
v2.1.0 at 24a849adcf99ca52b6d5897da4f8fe0e345f129b
v2.1.1 at 8766a793a9ebbb84e8faebdb311f78090144493b
v2.1.2 at 3fe6f11c26e353dbff0e50be7c21a8a734d1e5d1
v2.1.3 at 92ad639fa8524f9a3983b73346c97e947d57d55d
...
```

## Contribuer

Les correctifs sont avec plaisir appréciés et bienvenus.
Pour contribuer il est conseillé d'utiliser la version [git](https://git.spip.net/spip-contrib-outils/creer_tags.git).

## Git ou SVN ?

Le code est disponible sur :
* [git](https://git.spip.net/spip-contrib-outils/creer_tags.git)
* [svn](https://zone.spip.net/trac/spip-zone/browser/spip-zone/_outils_/creer_tags) .

Ces 2 versions sont synchronisées et par conséquent il est normalement possible de lire et écrire indifféremment sur les 2 dépots.
**Toutefois** c'est la version git qui sert de référence.
