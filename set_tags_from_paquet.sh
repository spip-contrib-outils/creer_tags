#!/bin/bash
#Documentation :
#grep : https://stackoverflow.com/questions/3717772/regex-grep-for-multi-line-search-needed
#
#
if [ -z "$1" ];then
	echo "Missing repository loca path to retag"
	exit 1
fi

repo_dir="$1"

if [ ! -d "$repo_dir" ];then
	echo "$repo_dir is not a directory"
	exit 1
fi

if [ ! -d "$repo_dir/.git" ];then
	echo "$repo_dir is not a git workspace"
	exit 1
fi

branch="master"
git="git -C $repo_dir"

version_previous=""

echo "$repo_dir"

for commit in $($git rev-list "$branch"|tac)
do
	version=""
	content=""

	#Find plugin xml first
	if $git ls-tree --name-only -r "$commit" |grep -q '^plugin.xml$'; then
		content=$($git show "$commit":plugin.xml|tr -d "\\r\\n\\0" 2>/dev/null)
		if [ "$content" ]; then
			version=$(echo "$content" | grep -oP "<\\s*version\\s*>\\s*\\K([0-9.]*)")
		fi
	fi

	#Override result by paquet.xml data
	if $git ls-tree --name-only -r "$commit" |grep -q '^paquet.xml$'; then
		content=$($git show "$commit":paquet.xml|tr -d "\\r\\n\\0" 2>/dev/null)
		if [ "$content" ]; then
			version=$(echo "$content" | grep -oP "<paquet.*version\\s*=\\s*.\\K[0-9.]*")
		fi
	fi

	# Set version
	if [ -n "$version" ] && [ "$version" != "$version_previous" ]; then
		$git tag v"$version" "$commit" 2>/dev/null
		echo "v$version at $commit"
		version_previous="$version"
	fi
done
